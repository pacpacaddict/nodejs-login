'use strict';

const controller = require('../controllers/controller');

module.exports = function(app) {
    // ---------------------
    // |    Main Routes    |
    // ---------------------
    // Gets
    app.route('/').get(controller.home);
    
    // Posts
    app.post('/user/register', controller.registerPost);


    // ---------------------
    // |    User Routes    |
    // ---------------------
    // Gets
    app.route('/user/register').get(controller.register);
    app.route('/user/userinfo').get(controller.userinfo);
    app.route('/user/login').get(controller.login);

    // Posts
    app.post('/user/login', controller.loginPost);

};