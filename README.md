## ISU NodeJS Development App ##

### **How it works:**
The app follows MVC Framework. It all begins shortly on server.js, which tells the application to begin listening on our chosen port (3000). From there, any request made is sent to the routes/routes.js file to decipher where in controller/controller.js to send the request. Controller.js contains several functions to either create and return a webpage, or to get and return some data on a webpage. Some parts of the controller will direct to the models in order to get and return information from the APIs.

### **Notes:** ###
 It is set up for two APIs, one for Ron Swanson quotes and another that determines the distance between two files. The application uses PUG (a Jade language) to generate wepages. PUG comes with partials that we use to implement several bits of HTML/JavaScript between several pages, allowing a change in one spot to affect several others.

### **What does it do:** ###

Upon opening the localhost:3000 (will be different once configured) on a web browser will open a simple webpage that is used to access and control a few test APIs. One of these is an API to generate and return a random Ron Swanson quote, which is loaded new at every page load. Another API is the distance between two zip codes, which has a form on the Distance page that sends a POST request to our own app to generate a response from an API that gives a calculation of distance between ZIPs. It will then print the distance upon completion once the page is reloaded with that info.