const e = require('express');
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

//Set View Engine
app.set('view engine', 'pug');
app.set('views','./views');
app.use(express.urlencoded({extended: true}));

//Load users
const controller = require('./controllers/controller');
controller.loadUsers()

//Begin Listening
const routes = require('./routes/routes');
routes(app);
app.listen(port, function() {
    console.log('Server started on port: ' + port);
})