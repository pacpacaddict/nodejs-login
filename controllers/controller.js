'use strict';

var properties = require('../package.json');

const fs = require('fs')

const users = []

var controllers = {
    //Get handlers
    home: function(req, res) {
        res.render('index')
    },
    register: function(req, res) {
        res.render('register')
    },
    userinfo: function(req, res) {
        res.json(users)
    },
    login: function(req, res) {
        res.render('login')
    },

    //Post handlers
    registerPost: function(req, res) {
        const user = { name: req.body.username, password: req.body.password}
        users.push(user)
        //Save Users
        fs.writeFileSync('./users.json', JSON.stringify(users))
        res.status(201)
        res.redirect("/user/login")
    },

    loginPost: function(req, res) {
        const user = users.find(user => user.username = req.body.username);
        if(user == null) {
            return res.status(400).send('User does not exist.');
        }
        try {
            if(user.password == req.body.password) {
                res.send('Login Success');
            } else {
                res.send('Not allowed');
            }
        } catch {
            res.status(500).send();
        }
    },
    loadUsers: function() {
        const file = "users.json"
        // Check that the file exists locally
        console.log("Test!!!")
        if(!fs.existsSync(file)) {
            console.log("File not found");
        } else {
            try {
                console.log('File Found! Attempting to load...')
                const usersToAdd = JSON.parse(fs.readFileSync(file, 'utf8'))
                usersToAdd.forEach(element => {
                    users.push(element)
                });
                return true
            } catch (err) {
                console.error(err)
                return false
            }
        }
    }
};

module.exports = controllers;